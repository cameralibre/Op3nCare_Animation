A number of short animations to illustrate themes related to the Op3nCare research project, used in the Op3nCare [introductory video](https://vimeo.com/162811723). 

Find out more about Op3nCare and how you can be part of it at [Op3ncare.cc](http://op3ncare.cc)


## Project structure:

### /sif/
[Synfig](http://synfig.org/) animation project files

### /svg/
[Inkscape](http://inkscape.org/) vector illustration project files

### /noun project/
Downloaded icons from [The Noun Project](http://nounproject.com/), used in the animation:

'[Head](https://thenounproject.com/term/head/184237/)' CC-BY Hunotika

'[Backpack](https://thenounproject.com/term/backpack/381941/)' CC-BY     Claire Skelly

'[Group](https://thenounproject.com/term/group/41751/)' CC-BY Meaghan Hendricks

'[DNA](https://thenounproject.com/term/dna/14696/)' CC-BY Sundance McClure

'[Health Care](https://thenounproject.com/term/health-care/188568/)' CC-BY Sergey Demushkin

'[Network](https://thenounproject.com/term/network/23949/)' CC0 by XOXO' 

### /webm/
Rendered videos of each animation.

